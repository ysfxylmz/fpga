#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xgpio.h"

#define led_0 0x01
#define led_1 0x02
#define led_2 0x04
#define led_3 0x08

XGpio gpio;

void driverInit() {
   int status;
   status = XGpio_Initialize(&gpio, XPAR_AXI_GPIO_0_DEVICE_ID);
   if (status != XST_SUCCESS) {
      print("Gpio initalization fail\n\r");
   } else {
      print("Gpio Initialization success\n\r");


   }
}

void configGpio() {
   XGpio_SetDataDirection(&gpio, 1, 0);
   XGpio_SetDataDirection(&gpio, 2, 1);
   XGpio_DiscreteSet(&gpio, 1, 0);
}

void runProject() {
   int button;
   while (1) {
      button = XGpio_DiscreteRead(&gpio, 2);
      switch (button) {
      case 1:
         xil_printf("%d: ",button);
         XGpio_DiscreteWrite(&gpio, 1, led_0);
         
         break;
      case 2:
         xil_printf("%d: ",button);
         XGpio_DiscreteWrite(&gpio, 1, led_1);
         
         break;
      case 4:
         xil_printf("%d: ",button);
         XGpio_DiscreteWrite(&gpio, 1, led_2);
         
         break;
      case 8:
         xil_printf("%d: ", button);
         XGpio_DiscreteWrite(&gpio, 1, led_3);
         break;
      default:
         XGpio_DiscreteClear(&gpio, 1, led_0);
         XGpio_DiscreteClear(&gpio, 1, led_1);
         XGpio_DiscreteClear(&gpio, 1, led_2);
         XGpio_DiscreteClear(&gpio, 1, led_3);

         break;

      }
   }
}
int main() {
   init_platform();


   driverInit();

   configGpio();

   runProject();

   cleanup_platform();
   return 0;
}
